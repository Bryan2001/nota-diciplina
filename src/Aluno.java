/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author DELL
 */
public class Aluno {
    private String nome;
    private float trabalho, prova1, prova2;
    private float nota1_p1 , nota2_p1, nota3_p1 ;
    private float media = -1;

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTrabalho(float trabalho) {
        this.trabalho = trabalho;
    }

    public void setProva1(float prova1) {
        this.prova1 = prova1;
    }

    public void setProva2(float prova2) {
        this.prova2 = prova2;
    }

    public void setNota1_p1(float nota1_p1) {
        this.nota1_p1 = nota1_p1;
    }

    public void setNota2_p1(float nota2_p1) {
        this.nota2_p1 = nota2_p1;
    }

    public void setNota3_p1(float nota3_p1) {
        this.nota3_p1 = nota3_p1;
    }

    public void setMedia(float media) {
        this.media = media;
    }

    public String getNome() {
        return nome;
    }

    public float getTrabalho() {
        return trabalho;
    }

    public float getProva1() {
        return prova1;
    }

    public float getProva2() {
        return prova2;
    }

    public float getNota1_p1() {
        return nota1_p1;
    }

    public float getNota2_p1() {
        return nota2_p1;
    }

    public float getNota3_p1() {
        return nota3_p1;
    }

    public float getMedia() {
        return media;
    }

    void registro()
{
     
       System.out.println("\n" + "\n");
       System.out.println(" Nome do aluno:" + nome + "\n Trabalho de programação 2:" + trabalho + "     Prova 1 de programação 2: " + prova1 +   "Prova 2 de programação 2: " + prova2 + "\n Nota do primeiro trimestre de Programação 2:" + media);
      
}
    
    void comparar()
    {
           if(nota1_p1<media)
                {
                     System .out.println("\n O aluno " + nome + " tem a nota de programação 2 maior do que a de programação 1!");
                }
           else if(nota1_p1>media)
                 {
                     System .out.println("\n O aluno " + nome + " tem a nota de programação 1 maior do que a de programação 2!");
                 }
           else
                 {
                     System .out.println("\n O aluno " + nome + " tem a mesma nota nos dois trimestre!");
            
                 } 
       }
    }
   

